## 0.10.0 (2022-07-22)

### other (1 change)

- [Add .task/ to gitignore](gitlab-org/cloud-native/gitlab-operator@318e1a386eca5970960e166dcab053b1efab9b26) ([merge request](gitlab-org/cloud-native/gitlab-operator!481))

## 0.9.3 (2022-07-19)

No changes.

## 0.9.2 (2022-07-05)

No changes.

## 0.9.1 (2022-06-30)

No changes.

## 0.9.0 (2022-06-22)

No changes.

## 0.8.2 (2022-06-16)

No changes.

## 0.8.1 (2022-06-01)

No changes.

## 0.8.0 (2022-05-22)

No changes.

## 0.7.2 (2022-05-05)

No changes.

## 0.7.1 (2022-05-02)

No changes.

## 0.7.0 (2022-04-22)

No changes.
